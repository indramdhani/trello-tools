<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="<?php echo ASSETS_URL?>assets/third-party/bootstrap/css/bootstrap.min.css" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo ASSETS_URL?>assets/third-party/bootstrap/css/bootstrap-reboot.min.css" crossorigin="anonymous">
	<title>Hello, world!</title>
</head>
<body class="bg-light">
	<div class="base-url d-none"><?php echo BASE_URL;?></div>
	<div class="container">
		<div class="py-5 text-center">
			<img class="d-block mx-auto mb-4" src="https://getbootstrap.com/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72">
			<h2><?php echo TITLE;?></h2>
			<p class="lead"><?php echo LEAD;?></p>
		</div>
		<div class="row">
			<div class='col-md-4 offset-md-4'>
				<button id="auth-trello" class="btn btn-info btn-lg btn-block" type="submit">Login to Trello</button>
			</div>
		</div>
		<footer class="my-5 pt-5 text-muted text-center text-small">
			<p class="mb-1">© 2017-2018 Company Name</p>
			<ul class="list-inline">
				<li class="list-inline-item"><a href="#">Privacy</a></li>
				<li class="list-inline-item"><a href="#">Terms</a></li>
				<li class="list-inline-item"><a href="#">Support</a></li>
			</ul>
		</footer>
	</div>
	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script  src="//code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="<?php echo ASSETS_URL?>assets/third-party/bootstrap/js/bootstrap.min.js" crossorigin="anonymous"></script>

	<script src="https://api.trello.com/1/client.js?key=<?php echo $this->config->item('trello_api_key');?>"></script>
	<script src="<?php echo ASSETS_URL?>assets/scripts/trello.js"></script>
</body>

</html>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tools extends Core_trello_tools {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}
	public function index()
	{
		$template["content"] = $this->load->view("pages/index.php");
		$this->load->view("layout-frontend");
	}

	public function board_list()
	{
		$this->load->view("layout-frontend");
	}

}

/* End of file Tools.php */
/* Location: ./application/modules/trello_tools/controllers/Tools.php */
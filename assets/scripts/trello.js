var trello_engine = {
	BASE_URL: $('.base-url').html(),
	init: function(){
		var self = this;
		self.setup_page();
		self.auth();
	},
	setup_page: function(){
		var self = this;
		var button = $("#auth-trello");
		if(Trello)
		{
			button.addClass('d-none');
			// window.location.href = 	self.BASE_URL + 'board-list';
		}
	},
	auth: function(){
		var self = this;
		var button = $("#auth-trello");
		button.on('click', function(event) {
			event.preventDefault();
			self.authenticate();
		});
	},
	authenticate: function(){
		var self = this;
		window.Trello.authorize({
			type: 'redirect',
			name: 'trello-tools',
			scope: {
				read: 'true',
				write: 'true',
				account: 'true',
			},
			expiration: '1hour',
			success: self.authenticationSuccess,
			error: self.authenticationFailure,
		});
	},
	authenticationSuccess: function(){
		window.Trello.members.get('me').done(function(data) {
			console.log(data);
		});
	},
	authenticationFailure: function (){
		console.log("failure");
	},
	debug: function(text){
		console.log(text);
	}
};

jQuery(document).ready(function($) {
	trello_engine.init();
});